module.exports = {
  title: 'XYLEM Handbook',
  description: 'The official XYLEM Handbook.',
  plugins: [['@vuepress/active-header-links'], ['vuepress-plugin-global-toc']],
  themeConfig: {
    
    logo: '/xylem-logo.png',
    lastUpdated: 'Last updated',
    repo: 'https://gitlab.com/pinewood-builders/handbooks/xylem/xylem-handbook',
    // docsDir: 'docs',
    editLinks: true,
    editLinkText: 'Recommend a change',
    yuu: {
      defaultDarkTheme: true,
    },
    displayAllHeaders: true,
    nav: [
      {
        text: 'Home',
        link: '/'
      },
      {
        text: 'Contact',
        items: [
          {
            text: 'XYLEM Discord',
            link: 'https://bit.ly/3aNd2uD'
          }
        ]
      },
    ],
    sidebar: {
      '/xylem/': ["building/", "scripting/", "points/"]
    },
  }
}
